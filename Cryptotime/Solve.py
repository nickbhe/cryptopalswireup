from binascii import b2a_base64, a2b_base64, unhexlify, hexlify
from bitstring import BitArray
import string


def main():
    with open('6.txt', 'r') as file:
        text = file.read()
    print(crack_repeating_key_xor(a2b_base64(text)))


def crack_repeating_key_xor(byte_string: bytes) -> bytes:
    key = find_repeating_xor_key(byte_string, find_xor_key_size(byte_string))
    return bytes_xor(byte_string, str.encode(create_repeated_key(key, len(byte_string))))


def find_repeating_xor_key(byte_string: bytes, key_length: int) -> str:
    input_blocks = [byte_string[i * key_length:(i + 1) * key_length]
                    for i in range((len(byte_string) + key_length - 1) // key_length)]
    transposed_blocks = [bytes([input_blocks[j][i] for j in range(len(input_blocks))
                                if not (i >= len(input_blocks[len(input_blocks) - 1]) and j == len(input_blocks) - 1)])
                         for i in range(len(input_blocks[0]))]
    return "".join([chr(letter_frequency_analysis(transposed_blocks[i])['key']) for i in range(len(transposed_blocks))])


def find_xor_key_size(byte_string: bytes) -> int:
    best_key_data = {
        'size': 2,
        'distance': 99999
    }
    max_key_size = 40
    for keysize in range(2, max_key_size):
        distances = [find_edit_distance((byte_string[keysize * i:keysize * (i + 1)]).hex(),
                                        byte_string[keysize * (i + 1):keysize * (i + 2)].hex()) / keysize
                     for i in range(int(len(byte_string) / keysize - 1))]
        distance = sum(distances) / len(distances)
        if distance < best_key_data['distance']:
            best_key_data['size'] = keysize
            best_key_data['distance'] = distance
    return best_key_data['size']


def find_edit_distance(first_hex_string: str, second_hex_string: str) -> int:
    return sum(a != b for a, b in zip(BitArray("0x" + first_hex_string), BitArray("0x" + second_hex_string)))


def create_repeated_key(key: str, length: int) -> str:
    return (key * (int(length / len(key)) + 1))[:length]


def letter_frequency_analysis(byte_string: bytes) -> dict:
    english_character_frequency = {'E': 12.70, 'T': 9.06, 'A': 8.17, 'O': 7.51, 'I': 6.97, 'N': 6.75, 'S': 6.33,
                                   'H': 6.09, 'R': 5.99, 'D': 4.25, 'L': 4.03, 'C': 2.78, 'U': 2.76, 'M': 2.41,
                                   'W': 2.36, 'F': 2.23, 'G': 2.02, 'Y': 1.97, 'P': 1.93, 'B': 1.29, 'V': 0.98,
                                   'K': 0.77, 'J': 0.15, 'X': 0.15, 'Q': 0.10, 'Z': 0.07, ' ': 35}
    best_xor_data = {
        'key': 0,
        'score': 0
    }

    for i in range(256):
        score = 0.0
        xored_bytes = bytes_xor(byte_string, bytearray(bytes([i]) * len(byte_string)))
        for letter in english_character_frequency:
            score += bytes_to_str(xored_bytes).upper().count(letter) * english_character_frequency[letter]
        if score > best_xor_data['score']:
            best_xor_data['score'] = score
            best_xor_data['key'] = i

    return best_xor_data


def get_string_by_letter_frequency(byte_string: bytes) -> bytes:
    return bytes_xor(byte_string, unhexlify(hex(letter_frequency_analysis(byte_string)['key'])[2:] * len(byte_string)))


def hex_to_base64(hex_string: str) -> str:
    return b2a_base64(unhexlify(hex_string))


def bytes_xor(first_buffer: bytes, second_buffer: bytes) -> bytes:
    return bytes([(a ^ b) for a, b in zip(first_buffer, second_buffer)])


def string_to_hex_string(s: str) -> str:
    return "".join("{:02x}".format(ord(c)) for c in s)


def bytes_to_str(byte_list: bytes) -> str:
    return "".join(filter(lambda x: x in string.printable, "".join(map(chr, byte_list))))


if __name__ == '__main__':
    main()
